import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {SupplierService} from './shared/supplier.service';
import {HomeComponent} from './home';
import {SuppliersComponent} from './components/suppliers/suppliers.component';

import '../style/app.scss';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'my-app', // <my-app></my-app>
  providers: [SupplierService],
  directives: [...ROUTER_DIRECTIVES],
  template: require('./app.component.html'),
  styles: [require('./app.component.scss')],
})
@RouteConfig([
  {path: '/', component: HomeComponent, name: 'Home'},
  {path: '/suppliers', component: SuppliersComponent, name: 'Suppliers'}
])
export class AppComponent {
  constructor() {
  }
}
